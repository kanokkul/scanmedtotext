Preprocessing
----
**‘offline handwriting skew line segmentation preprocessing’**
Off-line Handwriting Text Line Segmentation : A Review
+	https://s3.amazonaws.com/academia.edu.documents/45959713/20080703.pdf?response-content-disposition=inline%3B%20filename%3DOff-Line_Handwriting_Text_Line_Segmentat.pdf&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWOWYYGZ2Y53UL3A%2F20190618%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190618T051743Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=64dac5b157f7fea195b4fbee84154a99df1f695fe42f3d838ab197d4fa84bcc9

Annotated comparisons of proposed preprocessing techniques for script recognition
+	https://link.springer.com/article/10.1007/s00521-014-1618-9 

Neural networks for document image preprocessing: state of the art
+	https://link.springer.com/article/10.1007/s10462-012-9337-z 

**‘word separation handwritten lines’**

Separating lines of text in free-form handwritten historical documents
+	https://ieeexplore.ieee.org/abstract/document/1612942 





OCR
----

[https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4765700/]
+	Building Structured Personal Health Records from Photographs of Printed Medical Records
+	Xiang Li, PhD,1 Gang Hu, MS,1 Xiaofei Teng, PhD,1 and Guotong Xie, MS1
+ Aim: Convert Chinese printed medical record token by phone camera to structured personal health record
+ Control: human read data
+ Measurement: precision (positive predictive value), recall (sensitivity), F-measure (harmonic mean of PPV and Recall) of generated text which confident value is 0 and is gradually increased to 1


Facing Problem, Solution, and Result:
1.	Quality of photo from mobile phone: contrast, noise, skewness  Photo preprocessing: by OpenCV
1.1.	Binarization: convert to Black and White by adaptive threshold algorithm to avoid different light in different area
1.2.	Remove noise: non-local mean denoise algorithm – replace pixel with average color of most resembling in search window
1.3.	Deskew: IBM Datacap Taskmaster Capture – But not use in real program due to remove important stroke in Chinese letters
2.	Multiple language: Chinese + English
2.1.	Use multiple OCR to convert and combine the result  XML-based data containing character and confident value from 0 to 1
2.2.	Postprocessing
2.2.1.	Join the characters to word: find the punctuation e.g. dot or parenthesis
2.2.2.	Select between English or rather than Chinese word by three criteria – English has greater confident value, there are both English and Chinese word and length of English word is longer than another
3.	Annotation the document: to classify the raw data to structural data
3.1.	Document type: Keyword-based and Naïve Bayes Model from training dataset – term frequency and inverse document frequency
3.2.	Entity: 1. Dictionary-based  calculate the similarity  Entity candidate with Similarity; 2. Regex to extract value
3.3.	Correlation between entity and value: in the same line and value must always follow entity.
Adaptation
Technique of preprocess and post processing
Differences: printed words are in same pattern, no variation like handwriting, OCR can be used easily



[https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3392858/]
+	Development of an optical character recognition pipeline for handwritten form fields from an electronic health record
+	Luke V Rasmussen, corresponding author1 Peggy L Peissig,1 Catherine A McCarty,2 and Justin Starren1,3
+ Aim: Extract handwritten text in template form from scanned EMR

Facing Problem, Solution, and Result:
1.	Overlapping of text and template line, noise: preprocessing
1.1.	Overlapping of text & line of form: use LEADTOOLS (Yoo JY, et al. Line removal and restoration of handwritten characters on the form documents. ICDAR’97, IEEE Computer Society, Los Alamitos, CA, 1997:128e31) tool kit
1.2.	Diagrams, which are not text, some are overflow the boundary of forms: the areas of diagram were masked and then were removed in some run of pipeline.
2.	OCR processing
2.1.	Tesseract OCR, LEADTOOLS Intelligent Character Recognition (ICR) Module, OmniPage Capture SDK from Nuance Communications
3.	Postprocessing: to correct missed interpreted text
3.1.	Regular expression: r[1lL/\\]s  NS, [1lL/\\][t+]  1+, [Z][t+]  2+
3.2.	Approve the regular expression by valid each OCR tool in every run by human
3.3.	Combination the result between OCRs by AND or OR


~~~ [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2244242/] ~~~
+	A modern optical character recognition system in a real-world clinical setting: some accuracy and feasibility observations.
+	Paul G. Biondich, J. Marc Overhage, Paul R. Dexter, Stephen M. Downs, Larry Lemmon, and Clement J. McDonald
+	Design แค่วิธีการออกแบบฟอร์ม 

[https://www.researchgate.net/publication/228437787_Information_Extraction_from_Handwritten_Medical_Records_and_Assigning_ICD-10_Codes]
+	Information Extraction from Handwritten Medical Records and Assigning ICD-10 Codes
+	Pouya Foudeh, Naomie Salim
+ Aims: Extract handwritten medical record and assign to ICD-10 codes

**Facing problem & Solution:**
1.	Illegible handwriting and unintelligent grammar:
1.1.	Ontology: vocabulary --- from NCBO of NIHUS, MYCYN of Stanford, Waraporn by KMUTT
1.2.	Word weighting
1.2.1.	Remove stop word
1.2.2.	Weight by calculation
Difference: this paper is only methodology


[http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.446.1218&rep=rep1&type=pdf]
+ A Medical Knowledge Based Postprocessing Approach for Doctor’s Handwriting Recognition
+ Qi Chen†, Tianxia Gong†, Linlin Li†, Chew Lim Tan†, Boon Chuan Pang‡
+ Aims: convert online handwriting to medical word by knowledge based

**Facing problem & Solution:**
1.	Handwriting recognition: Return the set of candidate words
1.1.	Script partitioning: split between to word by time distance and space distance, some strokes i.e. t bar or “i” dot are ignore and use postprocessing to help
1.2.	Character recognition: segmentation algorithm and Lipi toolkit
1.3.	Word recognition: dictionary (search tree)
2.	Post processing unit
2.1.	Extract sentence into medical terminology  compare with database  return relevant word  compare with input word and generate new sentences
2.2.	Information extraction: extract signs and symptoms
2.2.1.	Transfer knowledge from medical DB to form a standard concept
2.2.1.1.	The knowledge engine contains the database which has medical terms and its relation.
2.2.2.	Convert the recognized text into the candidate concept
2.2.3.	Mapping medical terms in the candidate concept with the standard concept
2.3.	Restoration: use context vectors

Difference: this paper is online HW recognition


[https://cubs.buffalo.edu/images/pdf/pub/automatic-recognition-of-handwritten-medical-forms-for-search-engines.pdf]
+	AUTOMATIC RECOGNITION OF HANDWRITTEN MEDICAL FORMS FOR SEARCH ENGINES
+	Robert Jay Milewski
+ Aims: Reading handwritten Prehospital Care Report (PCR) to search database which has problem – spill text over boundary


