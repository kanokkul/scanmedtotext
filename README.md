Converting the Scanned Medical Record to Searchable Text
===

Problem
----
In Maharaj Nakorn Chiang Mai Hospital, the most of medical record, including patients’ history, physical examination, progress note, order form, nurse note, operative note etc., was recorded by handwriting and archived in scanned images. These records had the important information for patient care and researching. However, the method to search or extract them into valuable data was only opening and reading the scanned document by human, page by page. This task was inefficient and required time and human effort to do.

Impact
----
From Medical Records and statistics Section ,Faculty of Medicine, Chiang Mai University1, On June 20th 2019, there were ___(จำนวนผู้ป่วย)___ of registered patients in Maharaj Nakorn Chiang Mai Hospital. And there were ___(จำนวนรูป)___ pages of scanned medical record, and __(จำนวนรูป IPD)__ pages (_% of total) were IPD (in-patient department) records, approximately __(IPD page/จำนวน)__ pages per patient. In one day, about 130 patients were admitted per days, and their medical record needed to be reviewed by doctors. We could estimate that there were _____ pages of medical records read per days that took a lot of time to find out the important information. Furthermore, the resident doctors had to do researches. Some of them (__%__%) did the retrospective researches. So, they used a lot of their time to review the image medical records page by page. Therefor, if these records can be searchable, it may reduce time to look for some information and let the doctor focus on their patients longer.
Moreover, if we could extract the __(size)__ TB of scanned records into text, these data could be transformed easily into any format, which may have benefit on data mining and big data processing to get valuable knowledge from these enormous images. 

Related works
----
## Preprocessing of input images
Because the handwriting text has skew and noise. It needs to be preprocessing to improve the quality for better accuracy. There are multiple steps of preprocessing – binarization, noise elimination, field extraction, line removal, word segmentation and de-skewing – by various techniques2, 3.

## Processing
The handwritten words have a lot of variation even though same person. So, the convolution neural network is suitable method to transform the images into text. Some techniques use extracted characters to recognize4, 5. But others take the whole words as input and return the probable words without requiring character segmentation.
Wu et al5 compare the error rate of the conventional convolutional neural network (C-NN) with relaxation convolutional neural network (R-CNN),  alternately trained relaxation convolutional neural network (ATR-CNN) and human. Their training dataset contained 1,172,907 samples of Chinese characters from 300 different writers. And their testing set had 224,419 characters from 60 writers. The error rates of C-NN, R-CNN, ATR-CNN and human were 4.35%, 4.45%, 3.94% and 3.87%, respectively.
Dutta et al4 developed hybrid recurrent and convolutional neural network to improve the accuracy. They used 13,353 handwritten lines, containing 115,320 words from 657 writers. The character error rate and word error rate were lower than prior studies6, 7.  
Wang et al7 performed method to recognize text in scene images without character segmenting process by convolutional neural network and hidden Markov model. This research tried to develop both lexicon-based and non-lexicon-based models. 20,000 images and were randomly chosen to train the lexicon-based model. Then, they tested it by 2,000 random images. They also used 33,402 images of Street View House Number for training and 13,068 for testing the non-lexicon-based model. There were about 86.9-97.6% and 84-91% of accuracy for lexicon- and non-lexicon-based model, respectively.
Rabi et al6 also developed the method for Arabic handwriting recognition by hidden Markov model without segmentation. This model was trained by 26,459 handwritten words, three fourth for training and one fourth for testing. The recognition rate was 87.93%.


Objectives
----
1.	To develop the image-to-text machine learning model for handwriting medical record
2.	To implement the model with scanned IPD medical record storage systems for Maharaj Nakorn Chiang Mai Hospital in 2008 - 2018

Aims
----
1.	Accuracy of conversion is greater than 80%

Timelines
----
**June – July 2019**
Writing research proposal and submit to Research Administration Section and Research Ethic Committee of Faculty of Medicine, Chiang Mai University. And learning about prior researches, machine learning and correlated programming function.

**August – October 2019**
Gathering the medical records from Medical Records and Statistics Section of Maharaj Nakorn Chiang Mai Hospital. Developing and testing the image processing and machine learning model to reach goal.

**November 2019 – January 2020**
Writing the research report for publication


Material and Method
---- 
## Material, data and tools
1.	Input data for model training and testing from Maharaj Nakorn Chiang Mai Hospital on January 1st, 2008 – December 31st, 2018 which consists of …
1.1.	Scanned medical records
1.2.	Discharge summaries and drug orders
2.	Processing server computers with Python version 3.7 which only connect with private intranet of Maharaj Nakorn Chiang Mai

## Method
1.	Writing the processing code of machine learning model with Python language
2.	Run the code in processing server via local area network remote connection
3.	Preprocessing all input data by removal of patient identification profiles and image quality adjustment 
4.	Training the model by the input data in year 2008 – 2017
5.	Testing the model by the input data in year 2018 and measure the error
6.	Review the error and improve the model code and run the step 4 again

References
----
1.	Medical Records and statistics Section  FoM, Chiang Mai University. จำนวนการให้บริการผู้ป่วยนอกและผู้ป่วยใน [Webpage]. Medical Records and statistics Section ,Faculty of Medicine, Chiang Mai University.; 2019 [updated June 10, 2019. Available from: http://www.med.cmu.ac.th/hospital/medrec/2011/.
2.	Razak Z, Zulkiflee K, Idris M, Mohd Tamil E, Noorzaily Mohamed Noor M, Salleh R, et al. Off-Line Handwriting Text Line Segmentation: A Review2007.
3.	Saba T, Rehman A, Altameem A, Uddin M. Annotated comparisons of proposed preprocessing techniques for script recognition. Neural Computing and Applications. 2014;25(6):1337-47.
4.	Dutta K, Krishnan P, Mathew M, Jawahar CV. Improving CNN-RNN Hybrid Networks for Handwriting Recognition.  2018 16th International Conference on Frontiers in Handwriting Recognition (ICFHR)2018. p. 80-5.
5.	Wu C, Fan W, He Y, Sun J, Naoi S, editors. Handwritten Character Recognition by Alternately Trained Relaxation Convolutional Neural Network. 2014 14th International Conference on Frontiers in Handwriting Recognition; 2014 1-4 Sept. 2014.
6.	Rabi M, Amrouch M, Mahani Z. Cursive Arabic Handwriting Recognition System Without Explicit Segmentation Based on Hidden Markov Models. Journal of Data Mining and Digital Humanities. 2018;Special Issue on Scientific and Technological Strategic Intelligence (2016).
7.	Wang F, Guo Q, Lei J, Zhang J. Convolutional recurrent neural networks with hidden Markov model bootstrap for scene text recognition. IET Computer Vision. 2017;11(6):497-504.






